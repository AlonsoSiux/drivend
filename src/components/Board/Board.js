/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import './Board.css'
function Board({ dimension, color, shape }) {
	const [dimensionArr, setDimensionArr] = useState([])
	const [game, setGame] = useState([])
	const [player, setPlayer] = useState('p1')
	const [activePiece, setActivePiece] = useState(null)
	const [suggestedMoveL, setSuggestedMoveL] = useState(null)
	const [suggestedMoveR, setSuggestedMoveR] = useState(null)

	useEffect(() => {
		const dimensionArr = [...Array(dimension).keys()]
		setDimensionArr(dimensionArr)
		const game = [...Array(dimension ** 2).fill(null)]
		for (let i = 0; i < dimension * 2; i++) {
			game[i] = 'p1'
			game[game.length - 1 - i] = 'p2'
			setGame(game)
		}
		setGame(game)
	}, [dimension])

	const pieceMove = (num) => {
		if (!activePiece || game[num] === player) {
			// new move or new active piece
			readyToMove(num)
		} else if (!game[num] && (num===suggestedMoveL || num===suggestedMoveR)) {
			[game[num], game[activePiece]] = [game[activePiece], game[num]]
			setActivePiece(null)
			setSuggestedMoveL(null)
			setSuggestedMoveR(null)
			let nextPlayer = (game[num]==='p1') ? 'p2' : 'p1'
			setPlayer(nextPlayer)
		}
	}
	const readyToMove = (num) => {
		setActivePiece(num)
		let suggested1 = (game[num] === 'p1') ? (num+dimension+2) : (num-dimension+2)
		let suggested2 = (game[num] === 'p1') ? (num+dimension-2) : (num-dimension-2)
		suggested1 = (game[suggested1] || (num%dimension >= dimension-2)) ? null : suggested1
		suggested2 = (game[suggested2] || (num%dimension < 2)) ? null : suggested2
		setSuggestedMoveL(suggested1)
		setSuggestedMoveR(suggested2)
	}
	return (
		<div className="board">
			{dimensionArr.map((c) => {
				return (
					<div key={c} className={`col ${c % 2 === 0 ? 'starts-black' : 'starts-white'}`}>
						{dimensionArr.map(r => {
							let arrLen = dimensionArr.length
							let blockNumb = (c * arrLen) + r

							return (
								<button onClick={() => pieceMove(blockNumb)} key={r} className={`${(activePiece===blockNumb)? 'active-border':''} block`}>
									{(game[blockNumb]==='p1') && <span className={`${color === 'bright' ? 'red' : 'light-red'} ${shape === 'circle' ? 'circle' : 'triangle'}`}></span>}
									{(blockNumb === suggestedMoveL || blockNumb === suggestedMoveR) && <span className={`suggested circle`}></span>}
									{(game[blockNumb]==='p2') && <span className={`${color === 'bright' ? 'black' : 'mint'} ${shape === 'circle' ? 'circle' : 'triangle'}`}></span>}
								</button>
							)
						})}
					</div>
				)
			})}
		</div>
	)
}

export default Board

import React, { useState } from 'react'
import './App.css'
import Board from './components/Board/Board'
import ToolBar from './components/GameSettingsBar/GameSettingsBar'
function App() {
	const [dimension, setDimension] = useState(0)
	const [color, setColor] = useState('bright')
	const [shape, setShape] = useState('circle')

	const toggleColor = (e) => {
		setColor(e.target.value)
	}
	const toggleShape = (e) => {
		setShape(e.target.value)
	}
	const startNewGame = (dim) => {
		if (isNaN(dim) || dim<0) {
			setDimension(0)
		} else {
			setDimension(dim)
		}
	}

	return (
		<div className="App">
			<ToolBar dimension={dimension} color={color} shape={shape} onShapeChange={toggleShape} onColorChange={toggleColor} onChangeHandler={e => startNewGame(parseInt(e.target.value))}></ToolBar>
			<Board dimension={dimension} color={color} shape={shape}></Board>
		</div>
	);
}

export default App;
